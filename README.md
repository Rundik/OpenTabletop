# OpenTabletop
![DEPENDENCIES](https://david-dm.org/Rundik/OpenTabletop.svg)
[![GitHub issues](https://img.shields.io/github/issues/Rundik/OpenTabletop.svg?style=flat-square)](https://github.com/Rundik/OpenTabletop/issues)
[![GitHub stars](https://img.shields.io/github/stars/Rundik/OpenTabletop.svg?style=flat-square)](https://github.com/Rundik/OpenTabletop/stargazers)
[![HitCount](http://hits.dwyl.io/Rundik/OpenTabletop.svg)](http://hits.dwyl.io/Rundik/OpenTabletop)

Browser-based online project based on Vue.js and Firebase in which you can play any board games with your friends

Here is the demo website: [DEMO](https://playopentabletop.firebaseapp.com/)

## Features

  * Import/Export lobbies
  * Ingame chat
  * Any type of dices
  * Counters
  * Lobby customization
  * Storage for your decks

## Screenshots
<details>
  <summary>Monopoly</summary>
  
  ![Monopoly screenshot](https://i.imgur.com/L5OeLnk.png)
  ![Monopoly screenshot](https://i.imgur.com/rDReB0B.png)
  
</details>

<details>
  <summary>Magic: The Gathering</summary>
  
  ![MTG screenshot](https://i.imgur.com/I7JiH8q.png)
  ![MTG screenshot](https://i.imgur.com/cXZvlok.png)
  
</details>

<details>
  <summary>Munchkin Fallout [RUS]</summary>
  
  ![Munchkin Fallout screenshot](https://i.imgur.com/0WWnWp1.png)
  ![Munchkin Fallout screenshot](https://i.imgur.com/KbjsV3c.png)
  
</details>

<details>
  <summary>Carcassonne</summary>
  
  ![Carcassonne screenshot](https://i.imgur.com/BhEVCOi.png)
  ![Carcassonne screenshot](https://i.imgur.com/MlG6CZ4.png)
  
</details>

## Useful tools

  * [Imgur images getter](https://playopentabletop.firebaseapp.com/tools/imgurParser.html)
  * [MTG card urls getter](https://playopentabletop.firebaseapp.com/tools/mtgParser.html)
  * [Games library](https://playopentabletop.firebaseapp.com/tools/gamesLibrary.html)
  

## Development

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
